This project contains Vagrantfile to test installations and configurations of Jenkins in different frameworks.

Centos7 will be tested according to this [tutorial](https://www.howtoforge.com/tutorial/how-to-install-jenkins-automation-server-with-nginx-on-centos-7/)

Ubuntu will be tested according to this [tutorial](https://www.howtoforge.com/tutorial/ubuntu-jenkins-automation-server/)

To use this project clone the repo and run `vagrant up`. Connect to the machines via ssh.
To display the Jenkins Portal, visit
- Centos: 10.0.0.20:8080
- Ubuntu: 10.0.0.30:8080
